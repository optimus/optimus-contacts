<?php
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "strictly_positive_integer", "field": "contacts.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"categorie": { "type": "positive_integer", "field": "contacts.categorie", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 10 },
	"type": { "type": "positive_integer", "field": "contacts.type", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 25 },
	"firstname": { "type": "string", "field": "contacts.firstname", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"lastname": { "type": "string", "field": "contacts.lastname", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["emptytonull"], "default": null },
	"birth_date": { "type": "date", "field": "contacts.birth_date", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["emptytonull"], "default": null },
	"birth_zipcode": { "type": "string", "field": "contacts.birth_zipcode", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"birth_city": { "type": "string", "field": "contacts.birth_city", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"birth_city_name": { "type": "string", "field": "contacts.birth_city_name", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"birth_country": { "type": "positive_integer", "field": "contacts.birth_country", "default": null },
	"insee": { "type": "string", "field": "contacts.insee", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"marital_status": { "type": "positive_integer", "field": "contacts.marital_status", "default": 0 },
	"maiden_name": { "type": "string", "field": "contacts.maiden_name", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"company_name": { "type": "string", "field": "contacts.company_name", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"company_type": { "type": "positive_integer", "field": "contacts.company_type", "default": 0 },
	"company_capital": { "type": "string", "field": "contacts.company_capital", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"siren": { "type": "string", "field": "contacts.siren", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"company_greffe": { "type": "string", "field": "contacts.company_greffe", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"company_status": { "type": "positive_integer", "field": "contacts.company_status", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 0 },
	"address": { "type": "text", "field": "contacts.address", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"zipcode": { "type": "string", "field": "contacts.zipcode", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"city": { "type": "string", "field": "contacts.city", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"city_name": { "type": "string", "field": "contacts.city_name", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"country": { "type": "positive_integer", "field": "contacts.country", "default": 74 },
	"phone": { "type": "string", "field": "contacts.phone", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"fax": { "type": "string", "field": "contacts.fax", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"mobile": { "type": "string", "field": "contacts.mobile", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"email": { "type": "email", "field": "contacts.email", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"website": { "type": "string", "field": "contacts.website", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"pro_phone": { "type": "string", "field": "contacts.pro_phone", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"pro_fax": { "type": "string", "field": "contacts.pro_fax", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"pro_mobile": { "type": "string", "field": "contacts.pro_mobile", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"pro_email": { "type": "email", "field": "contacts.pro_email", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"pro_website": { "type": "string", "field": "contacts.pro_website", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"iban": { "type": "string", "field": "contacts.iban", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"bic": { "type": "string", "field": "contacts.bic", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"tva": { "type": "string", "field": "contacts.tva", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"notes": { "type": "text", "field": "contacts.notes", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"categorie_description" : { "type": "string", "field": "contacts_categories.value", "reference" : { "db" : "common.contacts_categories", "id" : "contacts_categories.id", "match" : "contacts.categorie" } },
	"type_description" : { "type": "string", "field": "contacts_types.value", "reference" : { "db" : "common.contacts_types", "id" : "contacts_types.id", "match" : "contacts.type" } },
	"company_type_description" : { "type": "string", "field": "company_types_lvl3.value", "reference" : { "db" : "common.company_types_lvl3", "id" : "company_types_lvl3.id", "match" : "contacts.company_type" } },
	"pays_description" : { "type": "string", "field": "pays.value", "reference" : { "db" : "common.pays", "id" : "pays.id", "match" : "contacts.country" } },
	"displayname" : { "type" : "string", "field": "contacts.displayname", "virtual": true }
}
', null, 512, JSON_THROW_ON_ERROR);

$get = function ()
{
	global $connection, $resource, $input;
	
	auth();
	allowed_origins_only();
		
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', false);
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	
	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
		if (isset($input->id))
		{
			$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts/' . $input->id);
			if (in_array('read', $restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire ce contact");
		}
		else
		{
			$restrictions = get_restrictions_list($input->user->id, $input->owner, 'contacts');
			if (sizeof($restrictions) > 0 AND array_count_values(array_column($restrictions, 0))['read'] == sizeof($restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lister les contacts de cet utilisateur");
		}
	//REQUETE SUR UN CONTACT IDENTIFIÉ
	if (isset($input->id))
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'contacts');
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Ce contact n'existe pas");
		else
			return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
	//REQUETE SUR TOUS LES CONTACTS AU FORMAT DATATABLES
	else 
	{	
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'contacts', $restrictions);
		$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
		
		if (is_array($restrictions))
		{
			for ($i=0; $i < sizeof($results); $i++)
				$results[$i]['restrictions'] = $restrictions['contacts/' . $results[$i]['id']] ?? array_diff($restrictions['contacts'],['create']);		
			$results = array_values(array_filter($results, fn ($result) => (!in_array('read', $result['restrictions']))));
		}
		
		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
};


$post = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	
	check_input_body($resource, 'post');
	
	$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts');
	if (in_array('create', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour créer un contact chez cet utilisateur");
	
	$query = datatables_insert($connection, $resource, 'user_' . $input->owner, 'contacts');
	if($query->execute())
	{
		$input->source = $input->body;
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'contacts');
		if (isset($input->source->email))
			$response = rest('https://api.' . getenv('DOMAIN') . '/optimus-webmail/' . $input->owner . '/contacts/' . $connection->lastInsertId(), 'PATCH', json_encode(array('firstname' => $results[0]['firstname'], 'lastname' => $results[0]['lastname'], 'company_name' => $results[0]['company_name'], 'email' => $results[0]['email'])), $_COOKIE['token']);
		if (isset($input->source->pro_email))
			$response = rest('https://api.' . getenv('DOMAIN') . '/optimus-webmail/' . $input->owner . '/contacts/' . $connection->lastInsertId(), 'PATCH', json_encode(array('firstname' => $results[0]['firstname'], 'lastname' => $results[0]['lastname'], 'company_name' => $results[0]['company_name'], 'pro_email' => $results[0]['pro_email'])), $_COOKIE['token']);
		return array("code" => 201, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
};


$patch = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	
	check_input_body($resource, 'patch');

	if (count(array_intersect(array_keys((array)$input->body),array_keys(array_filter((array)$resource, function($item){return !in_array('immutable', (array)$item->patch);})))) == 0) 
		return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");

	$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts/' . $input->id);
	if (in_array('write', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce contact");
	
	if (!exists($connection, 'user_' . $input->owner, 'contacts', 'id', $input->id))
		return array("code" => 404, "message" => "Ce contact n'existe pas");

	$query = datatables_update($connection, $resource, 'user_' . $input->owner, 'contacts', $input->id);
	if($query->execute())
	{
		$input->source = $input->body;
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'contacts');
		if (isset($input->source->email) OR isset($input->source->firstname) OR isset($input->source->lastname) OR isset($input->source->company_name))
			$response = rest('https://api.' . getenv('DOMAIN') . '/optimus-webmail/' . $input->owner . '/contacts/' . $input->id, 'PATCH', json_encode(array('firstname' => $results[0]['firstname'], 'lastname' => $results[0]['lastname'], 'company_name' => $results[0]['company_name'], 'email' => $results[0]['email'])), $_COOKIE['token']);
		if (isset($input->source->pro_email) OR isset($input->source->firstname) OR isset($input->source->lastname) OR isset($input->source->company_name))
			$response = rest('https://api.' . getenv('DOMAIN') . '/optimus-webmail/' . $input->owner . '/contacts/' . $input->id, 'PATCH', json_encode(array('firstname' => $results[0]['firstname'], 'lastname' => $results[0]['lastname'], 'company_name' => $results[0]['company_name'], 'pro_email' => $results[0]['pro_email'])), $_COOKIE['token']);
		return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
};


$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);

	$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts/' . $input->id);
	if (in_array('delete', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour supprimer ce contact");

	if (!exists($connection, 'user_' . $input->owner, 'contacts', 'id', $input->id))
		return array("code" => 404, "message" => "Ce contact n'existe pas");

	if (exists($connection, 'user_' . $input->owner, 'dossiers_intervenants', 'contact', $input->id))
		return array("code" => 400, "message" => "Ce contact ne peut pas être supprimé car il intervient dans un ou plusieurs dossiers");

	//A FAIRE : CHECKER si le contact apparait dans une facture
	//$factures_exists = $this->conn->query("SELECT id FROM optimus_user_1.interventions WHERE client = '" . $input->id . "' AND db IS NOT NULL")->rowCount();
	//if ($factures_exists > 0)
	//return array("code" => 400, "message" => "Ce contact ne peut pas être supprimé car des factures le concernant ont été émises");

	$contact_delete = $connection->query("DELETE FROM `" . 'user_' . $input->owner . "`.contacts WHERE id = '" . $input->id . "'");
	$authorization_delete = $connection->query("DELETE FROM `server`.`authorizations` WHERE owner = '" . $input->owner . "' AND resource = 'contacts/" . $input->id . "'");
	$response = rest('https://api.' . getenv('DOMAIN') . '/optimus-webmail/' . $input->owner . '/contacts/' . $input->id, 'DELETE', null, $_COOKIE['token']);
	return array("code" => 200);
};
?>
