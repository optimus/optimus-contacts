<?php
$get = function ()
{
	global $optimus_connection, $input;

	$input->siret = $input->path[2];
	validate('siret', $input->path[2], 'integer', true);

	if (!$input->siret)
		return array("code" => 400, "message" => "le paramètre 'siret' n'a pas été renseigné");
	
	if (strlen($input->siret) != 9)
		return array("code" => 400, "message" => "le paramètre 'siret' doit contenir 9 chiffres");

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, "https://data.inpi.fr/search");
	curl_setopt($curl, CURLOPT_POSTFIELDS, '
	{
		"query":
		{
			"type":"companies",
			"selectedIds":[],
			"sort":"relevance",
			"order":"asc",
			"nbResultsPerPage":"1",
			"page":"1",
			"filter":{},
			"q":"' . $input->siret  . '"
		},
		"aggregations":[]
	}');
	
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$rcsinfos = curl_exec($curl);
	$rcsinfos = json_decode($rcsinfos);

	if (sizeof($rcsinfos) == 0)
		return array("code" => 404, "message" => "Aucune donnée n'est disponible pour ce numéro siret");
	else
		return array("code" => 200, "data" => $rcsinfos);
};
?>