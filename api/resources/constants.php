<?php
include('libs/datatables.php');

$get = function ()
{
	global $connection, $resource, $input;
	
	auth();
	allowed_origins_only();
		
	$input->constant = check('id', $input->path[2], 'module', true);

	$resource = new stdClass();
	if (in_array($input->constant, array("company_types_lvl1", "company_types_lvl2", "company_types_lvl3", "contacts_categories", "contacts_relatives_types", "contacts_representatives_types", "contacts_company_statuses", "contacts_marital_statuses", "contacts_types", "greffes_rcs", "pays")))
	{
		$resource->id = (object) array("type" => "strictly_positive_integer", "field" => $input->constant . ".id");
		$resource->value = (object) array("type" => "string", "field" => $input->constant . ".value");
	}
	else if ($input->constant == "communes")
	{
		$resource->commune_insee = (object) array("type" => "string", "field" => "communes.commune_insee");
		$resource->code_postal = (object) array("type" => "string", "field" => "communes.code_postal");
		$resource->nom = (object) array("type" => "string", "field" => "communes.nom");
		$resource->ancien_nom = (object) array("type" => "string", "field" => "communes.ancien_nom");
		$resource->latitude = (object) array("type" => "decimal", "field" => "communes.latitude");
		$resource->longitude = (object) array("type" => "decimal", "field" => "communes.longitude");
	}
	else
		return array("code" => 404, "message" => "La constante demandée n'existe pas");

	$results = datatable_request($connection, (object) $resource, 'common', $input->constant);
	$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
	$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
	
	return array("code" => 200, "data" => $results, "last_row" => $last_row, "last_page" => $last_page);
};
?>