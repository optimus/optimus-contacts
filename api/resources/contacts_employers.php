<?php
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "positive_integer", "field": "contacts_employers.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"employee": { "type": "strictly_positive_integer", "field": "contacts_employers.employee", "post": ["required"], "patch": ["ignored"], "default": 0 },
	"employer": { "type": "strictly_positive_integer", "field": "contacts_employers.employer", "post": ["required", "notnull", "notempty"], "patch": ["notnull", "notempty"], "default": 0 },
	"displayname" : { "type": "string", "field": "contacts.displayname", "reference" : { "db" : "user_{owner}.contacts", "id" : "contacts.id", "match" : "contacts_employers.employer" } },
	"job_title": { "type": "string", "field": "contacts_employers.job_title", "post": ["emptytonull"], "patch": ["emptytonull"], "default": "" },
	"arrival": { "type": "date", "field": "contacts_employers.arrival", "post": ["emptytonull"], "patch": ["emptytonull"], "default": "" },
	"departure": { "type": "date", "field": "contacts_employers.departure", "post": ["emptytonull"], "patch": ["emptytonull"], "default": "" }
}
', null, 512, JSON_THROW_ON_ERROR);

$get = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$resource->displayname->reference->db = str_replace('{owner}', $input->owner, $resource->displayname->reference->db);

	check_input_body($resource, 'get');

	$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts/' . $input->id);
	if (in_array('read', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour voir ce contact");

	$employee = $connection->query("SELECT type FROM `user_" . $input->owner . "`.`contacts` WHERE id = '" . $input->id . "'")->fetchObject();
	if (!$employee)
		return array("code" => 404, "message" => "Ce contact employé n'existe pas");
	else if ($employee->type != 25)
		return array("code" => 400, "message" => "Ce contact employé n'est pas une personne physique");

	$input->body = json_decode('{"sort": [{"field": "arrival", "dir": "asc"}], "filter": [{"field": "employee", "type":"=", "value":"' . $input->id . '"}]}', null, 512, JSON_THROW_ON_ERROR);
	$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'contacts_employers', $restrictions);
	foreach ($results as &$result)
		unset($result['employee']);
	return array("code" => 200, "data" => sanitize($resource, $results), "restrictions" => $restrictions);
};


$post = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$resource->displayname->reference->db = str_replace('{owner}', $input->owner, $resource->displayname->reference->db);
	$input->body->employee = $input->id;

	check_input_body($resource, 'post');

	$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts/' . $input->id);
	if (in_array('write', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce contact");

	$employee = $connection->query("SELECT type FROM `user_" . $input->owner . "`.`contacts` WHERE id = '" . $input->id . "'")->fetchObject();
	if (!$employee)
		return array("code" => 404, "message" => "Ce contact employé n'existe pas");
	else if ($employee->type != 25)
		return array("code" => 400, "message" => "Ce contact employé n'est pas une personne physique");

	if (!exists($connection, 'user_' . $input->owner, 'contacts', 'id', $input->body->employer))
		return array("code" => 400, "message" => "L'employeur que vous tentez d'ajouter n'existe pas");
	
	if (isset($input->body->departure) AND strtotime($input->body->arrival) > strtotime($input->body->departure))
		return array("code" => 400, "message" => "La date d'entrée doit se situer avant la date de sortie");

	$query = datatables_insert($connection, $resource, 'user_' . $input->owner, 'contacts_employers');
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'contacts_employers');
		unset($results[0]['employee']);
		return array("code" => 201, "data" => sanitize($resource, $results[0]), "message" => "Employeur créé avec succès");
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
};


$patch = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->subid = check('subid', $input->path[5], 'strictly_positive_integer', true);
	$resource->displayname->reference->db = str_replace('{owner}', $input->owner, $resource->displayname->reference->db);

	check_input_body($resource, 'patch');

	$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts/' . $input->id);
	if (in_array('write', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce contact");

	$employee = $connection->query("SELECT type FROM `user_" . $input->owner . "`.`contacts` WHERE id = '" . $input->id . "'")->fetchObject();
	if (!$employee)
		return array("code" => 404, "message" => "Ce contact employé n'existe pas");
	else if ($employee->type != 25)
		return array("code" => 400, "message" => "Ce contact employé n'est pas une personne physique");

	if ($input->body->employer AND !exists($connection, 'user_' . $input->owner, 'contacts', 'id', $input->body->employer))
		return array("code" => 400, "message" => "L'employeur que vous tentez de modifier n'existe pas");

	$exists = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`contacts_employers` WHERE id = '" . $input->subid . "'")->fetchObject();
	if (!$exists)
		return array("code" => 400, "message" => "Cet employeur n'existe pas");

	$arrival = $input->body->arrival ?: $exists->arrival;
	$departure = $input->body->departure ?: $exists->departure;
	if ($departure AND strtotime($arrival) > strtotime($departure))
		return array("code" => 400, "message" => "La date d'entrée doit se situer avant la date de sortie");
	
	$query = datatables_update($connection, $resource, 'user_' . $input->owner, 'contacts_employers', $input->subid);
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->subid . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'contacts_employers');
		unset($results[0]['employee']);
		return array("code" => 200, "data" => sanitize($resource, $results[0]), "message" => "Représentant modifié avec succès");
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
};


$delete = function ()
{
	global $connection, $input, $domain;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->subid = check('subid', $input->path[5], 'strictly_positive_integer', true);
	$resource->displayname->reference->db = str_replace('{owner}', $input->owner, $resource->displayname->reference->db);

	$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts/' . $input->id);
	if (in_array('write', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce contact");

	if (!exists($connection, 'user_' . $input->owner, 'contacts', 'id', $input->id))
		return array("code" => 400, "message" => "Ce contact employé n'existe pas");

	if (!exists($connection, 'user_' . $input->owner, 'contacts_employers', 'id', $input->subid))
		return array("code" => 400, "message" => "Cet employeur n'existe pas");
	
	$delete = $connection->query("DELETE FROM `user_" . $input->owner . "`.`contacts_employers` WHERE id = '" . $input->subid . "'");
	return array("code" => 200);
};
?>