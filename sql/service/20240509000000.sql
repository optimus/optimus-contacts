USE `common`;

CREATE TABLE IF NOT EXISTS `contacts_representatives_types` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT INTO `contacts_representatives_types` VALUES (10, "Gérant");
INSERT INTO `contacts_representatives_types` VALUES (20, "Président");
INSERT INTO `contacts_representatives_types` VALUES (30, "Directeur général");
INSERT INTO `contacts_representatives_types` VALUES (40, "Président du conseil d'administration");
INSERT INTO `contacts_representatives_types` VALUES (45, "Membre du conseil d'administration");
INSERT INTO `contacts_representatives_types` VALUES (50, "Président du conseil de surveillance");
INSERT INTO `contacts_representatives_types` VALUES (55, "Membre du conseil de surveillance");
INSERT INTO `contacts_representatives_types` VALUES (60, "Président du directoire");
INSERT INTO `contacts_representatives_types` VALUES (65, "Membre du directoire");
INSERT INTO `contacts_representatives_types` VALUES (70, "Commissaire aux comptes titulaire");
INSERT INTO `contacts_representatives_types` VALUES (75, "Commissaire aux comptes suppléant");
INSERT INTO `contacts_representatives_types` VALUES (80, "Mandataire ad hoc");
INSERT INTO `contacts_representatives_types` VALUES (90, "Administrateur provisoire");
INSERT INTO `contacts_representatives_types` VALUES (100, "Administrateur judiciaire");
INSERT INTO `contacts_representatives_types` VALUES (110, "Liquidateur judiciaire");
INSERT INTO `contacts_representatives_types` VALUES (120, "Mandataire judiciaire");



CREATE TABLE IF NOT EXISTS `contacts_relatives_types` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `contacts_relatives_types` VALUES (10, "Enfant");
INSERT INTO `contacts_relatives_types` VALUES (20, "Epoux");
INSERT INTO `contacts_relatives_types` VALUES (30, "Partenaire de PACS");
INSERT INTO `contacts_relatives_types` VALUES (40, "Concubin");