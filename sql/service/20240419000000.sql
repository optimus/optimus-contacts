USE `common`;

CREATE TABLE IF NOT EXISTS `contacts_company_statuses` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT INTO `contacts_company_statuses` VALUES (10,'Mandat ad hoc');
INSERT INTO `contacts_company_statuses` VALUES (20,'Sauvegarde de justice');
INSERT INTO `contacts_company_statuses` VALUES (30,'Redressement judiciaire');
INSERT INTO `contacts_company_statuses` VALUES (40,'Liquidation judiciaire');