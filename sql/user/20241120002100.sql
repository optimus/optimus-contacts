CREATE TABLE IF NOT EXISTS `contacts_representatives` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`contact` int(10) unsigned NOT NULL,
	`representative` int(10) unsigned NOT NULL,
	`type` int(4) DEFAULT 0,
	`arrival` date DEFAULT NULL,
	`departure` date DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	KEY `contacts_to_representatives` (`contact`),
	CONSTRAINT `contacts_to_representatives` FOREIGN KEY (`contact`) REFERENCES `contacts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `contacts_shareholders` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`contact` int(10) unsigned NOT NULL,
	`shareholder` int(10) unsigned NOT NULL,
	`arrival` date DEFAULT NULL,
	`departure` date DEFAULT NULL,
	`bare` int(10) unsigned DEFAULT NULL,
	`usufruct` int(10) unsigned DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	KEY `contacts_to_shareholders` (`contact`),
	CONSTRAINT `contacts_to_shareholders` FOREIGN KEY (`contact`) REFERENCES `contacts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `contacts_employers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee` int(10) unsigned NOT NULL,
  `employer` int(10) unsigned NOT NULL,
  `job_title` varchar(127) DEFAULT NULL,
  `arrival` date DEFAULT NULL,
  `departure` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contacts_to_shareholders` (`employee`) USING BTREE,
  CONSTRAINT `contacts_to_shareholders_copy` FOREIGN KEY (`employee`) REFERENCES `contacts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `contacts_relatives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contact` int(10) unsigned NOT NULL,
  `relative` int(10) unsigned NOT NULL,
  `type` int(4) DEFAULT 0,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `contacts_to_shareholders` (`contact`) USING BTREE,
  CONSTRAINT `contacts_to_relatives` FOREIGN KEY (`contact`) REFERENCES `contacts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;