export default class OptimusContactsService
{
	constructor()
	{
		this.name = 'optimus-contacts'
		this.resources = [
			{
				id: 'contacts',
				name: 'contacts',
				description: 'Tous les contacts',
				path: 'contacts'
			},
			{
				id: 'contact',
				name: 'contact',
				description: 'Un contact déterminé',
				path: 'contacts/id',
				endpoint: '/optimus-contacts/{owner}/contacts'
			}
		]
		this.swagger_modules =
			[
				{
					id: "optimus-contacts",
					title: "OPTIMUS CONTACTS",
					path: "/services/optimus-contacts/optimus-contacts.yaml",
					filters: ["resources", "constants", "contacts", "service", "tools"]
				}
			]
		store.services.push(this)
	}

	login()
	{
		leftmenu.create('optimus-cloud', 'CLOUD')
		leftmenu['optimus-cloud'].add('contacts', 'Contacts', 'fas fa-address-book', () => router('optimus-contacts/contacts'))

		store.resources = store.resources.concat(this.resources)
	}

	logout()
	{
		leftmenu['optimus-cloud'].querySelector('#contacts').remove()

		for (let resource of this.resources)
			store.resources.splice(store.resources.findIndex(item => item.id == resource.id), 1)

		store.services = store.services.filter(service => service.name != this.id)
	}

	async global_search(search_query)
	{
		return [
			{
				icon: 'fas fa-address-book',
				title: 'Contacts',
				data: rest(store.user.server + '/optimus-contacts/' + store.user.id + '/contacts', 'GET', { filter: [{ field: "*", type: "like", value: search_query }], references: true, size: 12, page: 1 })
					.then(contacts => contacts?.data?.map(contact => { return { displayname: contact.displayname, route: 'optimus-contacts/contacts/editor?owner=' + store.user.id + '&id=' + contact.id + '#general' } }))
			}
		]
	}

	dashboard()
	{
		//if (!document.getElementById('quickaction-cloud-header'))
		//quickactions_add('quickaction-cloud-header', 'header', 'CLOUD')
		//quickactions_add('quickaction-cloud-contact-create', 'item', 'Ajouter un contact', 'fas fa-user-plus', () => quickactionContactCreate())
	}
}