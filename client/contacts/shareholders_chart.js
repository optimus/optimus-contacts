
export default class OptimusContactsCompositionTab
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		let shareholders = await rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/shareholders').then(response => response.data)
		let total_bare = shareholders.reduce((a, b) => a + b.bare, 0)
		let total_usufruct = shareholders.reduce((a, b) => a + b.usufruct, 0)

		await load('/services/optimus-contacts/contacts/shareholders_chart.html', this.target)
		main.querySelector('#tab_composition').classList.remove('is-hidden')

		if (total_bare > 0)
		{
			let column = document.createElement('div')
			if (total_usufruct > 0)
				column.classList.add('column', 'is-6')
			else
				column.classList.add('column', 'is-10', 'is-offset-1')
			this.target.querySelector('.columns').appendChild(column)

			this.chart_bare = await load('/components/optimus_chart.js', column,
				{
					type: 'pie',
					data: {
						labels: shareholders.filter(shareholder => shareholder.bare > 0).map(shareholder => shareholder.displayname),
						datasets: [{
							label: '   Détention (%) ',
							data: shareholders.filter(shareholder => shareholder.bare > 0).map(shareholder => (100 * shareholder.bare / total_bare).toFixed(2)),
							backgroundColor: ["#36A2EB", "#FF6384", "#4BC0C0", "#FF9F40", "#9966FF", "#FFCD56", "#C9CBCF", "#673B95", "#CCA6AC", "#FA573C", "#FBE983", "#16A765", "#4986E7", "#CD74E6", "#F691B2", "#FF7537", "#B3DC6C", "#42D692", "#9A9CFF", "#AC725E", "#F83A22", "#FFAD46", "#7BD148", "#9FE1E7", "#B99AFF", "#C2C2C2", "#D06B64", "#FAD165", "#92E1C0", "#9FC6E7", "#A47AE2", "#CABDBF"],
						}]
					},
					options: {
						plugins: {
							title: {
								display: (total_usufruct > 0),
								text: "NUE PROPRIETE",
								font: {
									size: 16
								},
							},
							legend: {
								position: 'bottom',
								title: {
									display: true,
									text: ""
								}
							}
						},
					}
				})
		}

		if (total_usufruct > 0)
		{
			let column = document.createElement('div')
			column.classList.add('column', 'is-half')
			this.target.querySelector('.columns').appendChild(column)

			this.chart_usufruct = await load('/components/optimus_chart.js', column,
				{
					type: 'pie',
					data: {
						labels: shareholders.filter(shareholder => shareholder.usufruct > 0).map(shareholder => shareholder.displayname),
						datasets: [{
							label: '   Détention (%) ',
							data: shareholders.filter(shareholder => shareholder.usufruct > 0).map(shareholder => (100 * shareholder.usufruct / total_usufruct).toFixed(2)),
							backgroundColor: ["#36A2EB", "#FF6384", "#4BC0C0", "#FF9F40", "#9966FF", "#FFCD56", "#C9CBCF", "#673B95", "#CCA6AC", "#FA573C", "#FBE983", "#16A765", "#4986E7", "#CD74E6", "#F691B2", "#FF7537", "#B3DC6C", "#42D692", "#9A9CFF", "#AC725E", "#F83A22", "#FFAD46", "#7BD148", "#9FE1E7", "#B99AFF", "#C2C2C2", "#D06B64", "#FAD165", "#92E1C0", "#9FC6E7", "#A47AE2", "#CABDBF"],
						}]
					},
					options: {
						plugins: {
							title: {
								display: true,
								text: "USUFRUIT",
								font: {
									size: 16
								},
							},
							legend: {
								position: 'bottom',
								title: {
									display: true,
									text: ""
								}
							}
						}
					}
				})
		}
	}

	onUnload()
	{
		if (this.chart_usufruct)
			this.chart_usufruct.chart.destroy()
		if (this.chart_bare)
			this.chart_bare.chart.destroy()
	}
}