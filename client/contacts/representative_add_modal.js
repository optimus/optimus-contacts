export default class OptimusContactsRepresentativeAddModal
{
	constructor(target, params) 
	{
		this.target = target
		this.component = params
	}

	async init()
	{
		await load('/services/optimus-contacts/contacts/representative_add_modal.html', this.target)
		await load('/components/optimus_form.js', modal.querySelector('.representative-block'))
		modal.querySelector('.representative-arrival').value = new Date().toISOString().slice(0, 10)
		await populate(modal.querySelector('.representative-type'), 'https://' + this.component.server + '/optimus-contacts/constants/contacts_representatives_types')
		setTimeout(() => modal.querySelector('.representative').focus(), 10)

		modal.querySelector('.user-edit.button').onclick = () => 
		{
			if (modal.querySelector('.representative').dataset.id == 'null')
				return false
			modal.hide()
			router('optimus-contacts/contacts/editor?owner=' + this.component.owner + '&id=' + modal.querySelector('.representative').dataset.id + '#general')
		}

		modal.querySelector('.user-create.button').onclick = () =>
			rest('https://' + this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts', 'POST')
				.then(response => 
				{
					if (response.code == 201)
					{
						modal.querySelector('.representative').value = response.data.displayname
						modal.querySelector('.representative').dataset.new_id = response.data.id
						modal.hide()
						router('optimus-contacts/contacts/editor?owner=' + this.component.owner + '&id=' + modal.querySelector('.representative').dataset.new_id + '#general')
					}
				})

		modal.onshow = () => setTimeout(() =>
			rest('https://' + this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts/' + (modal.querySelector('.representative').dataset.new_id || modal.querySelector('.representative').dataset.id))
				.then(response => 
				{
					modal.querySelector('.representative').value = response.data.displayname
					modal.querySelector('.representative').dataset.id = modal.querySelector('.representative').dataset.new_id
					modal.querySelector('.representative').classList.remove('is-danger')
				}), 50)

		modal.querySelector('.add-button').onclick = () =>
		{
			let data =
			{
				contact: this.component.id,
				representative: modal.querySelector('.representative').dataset.id,
				type: modal.querySelector('.representative-type').value,
				arrival: modal.querySelector('.representative-arrival').value,
				departure: modal.querySelector('.representative-departure').value,
			}
			rest(this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts/' + this.component.id + '/representatives', 'POST', data, 'toast')
				.then(response => 
				{
					if (response.code == 201)
					{
						this.component.add_representative(response.data, true)
						modal.close()
					}
				})
		}

		modal.querySelector('.representative-arrival-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.representative-arrival') })
		modal.querySelector('.representative-departure-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.representative-departure') })

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}