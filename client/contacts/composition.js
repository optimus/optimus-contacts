export default class OptimusContactsCompositionTab
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		if (window.location.href.split('#')[1] == 'composition/chart')
			return load('/services/optimus-contacts/contacts/shareholders_chart.js', this.target)

		loader(this.target, true)

		await load('/services/optimus-contacts/contacts/composition.html', this.target)
		main.querySelector('#tab_composition').classList.remove('is-hidden')

		Promise.all(
			[
				rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/representatives')
					.then(response => 
					{
						for (let representative of response.data)
							this.add_representative(representative)
					}),
				rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/shareholders')
					.then(response => 
					{
						for (let shareholder of response.data)
							this.add_shareholder(shareholder)
					})
			])
			.then(() => loader(this.target, false))

		this.target.querySelector('.representatives-add-button').onclick = () => modal.open('/services/optimus-contacts/contacts/representative_add_modal.js', false, this)
		this.target.querySelector('.shareholders-add-button').onclick = () => modal.open('/services/optimus-contacts/contacts/shareholder_add_modal.js', false, this)
	}

	async add_representative(representative, animation)
	{
		const template = document.getElementById('representative').content.firstElementChild.cloneNode(true)

		for (const element of template.querySelectorAll("input,select,textarea"))
			element.setAttribute('data-endpoint', this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/representatives/' + representative.id)
		await populate(template.querySelector('[data-field="type"]'), 'https://' + this.server + '/optimus-contacts/constants/contacts_representatives_types', false, false, representative.type)
		await load('/components/optimus_form.js', template, representative)

		template.querySelector('[data-field="displayname"]').onmouseover = function () { this.classList.add('is-underlined') }
		template.querySelector('[data-field="displayname"]').onmouseleave = function () { this.classList.remove('is-underlined') }
		template.querySelector('[data-field="displayname"]').onclick = () => router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + representative.representative + '#general')

		template.querySelector('.representative-delete-button').onmouseover = function () { template.querySelector('.message-header').classList.add('has-background-danger') }
		template.querySelector('.representative-delete-button').onmouseleave = function () { template.querySelector('.message-header').classList.remove('has-background-danger') }

		template.querySelector('.representative-delete-button').onclick = () =>
			rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/representatives/' + representative.id, 'DELETE')
				.then(response => 
				{
					if (response.code == 200)
					{
						template.classList.add('animate__animated', 'animate__zoomOut', 'animate__faster')
						template.addEventListener('animationend', () => template.remove())
					}
				})

		if (animation)
			template.classList.add('animate__animated', 'animate__zoomIn', 'animate__faster')

		main.querySelector('.representatives').appendChild(template)
	}

	async add_shareholder(shareholder, animation)
	{
		const template = document.getElementById('shareholder').content.firstElementChild.cloneNode(true)

		for (const element of template.querySelectorAll("input,select,textarea"))
			element.setAttribute('data-endpoint', this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/shareholders/' + shareholder.id)
		await load('/components/optimus_form.js', template, shareholder)

		template.querySelector('[data-field="displayname"]').onmouseover = function () { this.classList.add('is-underlined') }
		template.querySelector('[data-field="displayname"]').onmouseleave = function () { this.classList.remove('is-underlined') }
		template.querySelector('[data-field="displayname"]').onclick = () => router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + shareholder.shareholder + '#general')

		template.querySelector('.shareholder-delete-button').onmouseover = function () { template.querySelector('.message-header').classList.add('has-background-danger') }
		template.querySelector('.shareholder-delete-button').onmouseleave = function () { template.querySelector('.message-header').classList.remove('has-background-danger') }

		template.querySelector('.shareholder-delete-button').onclick = () =>
			rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/shareholders/' + shareholder.id, 'DELETE')
				.then(response => 
				{
					if (response.code == 200)
					{
						template.classList.add('animate__animated', 'animate__zoomOut', 'animate__faster')
						template.addEventListener('animationend', () => template.remove())
					}
				})

		if (animation)
			template.classList.add('animate__animated', 'animate__zoomIn', 'animate__faster')

		main.querySelector('.shareholders').appendChild(template)

		main.querySelector('.shareholders-chart-button').onclick = () => router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + this.id + '#composition/chart', true, this.target)
		main.querySelector('.shareholders-chart-button').classList.remove('is-hidden')
	}

	onUnload()
	{
		delete store.history
		main.querySelector('.undo-button').classList.add('is-hidden')
	}
}