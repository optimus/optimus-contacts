export default class contactBodacc
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		document.querySelector('#tab_bodacc').classList.remove('is-hidden')

		await load('/services/optimus-contacts/contacts/bodacc.html', this.target)
		main.querySelector('#bodacc').classList.remove('is-hidden')

		rest(store.user.server + '/optimus-contacts/' + store.queryParams.owner + '/contacts/' + store.queryParams.id, 'GET', {})
			.then(data =>
			{
				if (data.data.siren)
				{
					const url = 'https://bodacc-datadila.opendatasoft.com/api/records/1.0/search/?dataset=annonces-commerciales&q=' + data.data.siren

					fetch(url)
						.then(response => response.json())
						.then(annonces =>
						{
							if (annonces.records.length === 0)
							{
								document.getElementById('no-bodacc').classList.remove('is-hidden')
								document.getElementById('no-bodacc-message').textContent = "Aucune annonce publiée"
							}
							else
							{
								document.querySelector('#bodacc-container').innerHTML = '<br/><div>' + annonces.records.length + ' ANNONCE(S) PUBLIEE(S) AU BODACC</div><br/>'

								const table = document.createElement('table')
								table.classList.add('table', 'is-striped', 'is-bordered')
								document.querySelector('#bodacc-container').appendChild(table)

								const thead = table.createTHead()
								let tr = thead.insertRow()
								tr.insertCell().innerHTML = 'DATE'
								tr.insertCell().innerHTML = 'CATEGORIE'
								tr.insertCell().innerHTML = 'GREFFE DE DEPOT'
								tr.insertCell().innerHTML = 'LIEN'

								const tbody = table.createTBody()

								annonces.records.sort((a, b) => parseInt(b.fields.dateparution) - parseInt(a.fields.dateparution))

								for (let annonce of annonces.records)
								{
									const item = annonce.fields
									tr = tbody.insertRow()
									tr.insertCell().innerHTML = new Date(item.dateparution).toLocaleDateString()
									tr.insertCell().innerHTML = item.familleavis_lib
									tr.insertCell().innerHTML = item.tribunal
									tr.insertCell().innerHTML = '<a target="_blank" onclick="window.open(\'https://www.bodacc.fr/pages/annonces-commerciales-detail/?q.id=id:' + item.id + '\')"><i class="fa-solid fa-up-right-from-square"></i></a>'
								}
								document.getElementById('siren-missing').style = 'display : none'
								document.getElementById('no-bodacc').style = 'display : none'
							}
						})
				}
				else
				{
					document.getElementById('siren-missing').classList.remove('is-hidden')
					document.getElementById('no-bodacc').style = 'display : none'
				}
			})
	}
}