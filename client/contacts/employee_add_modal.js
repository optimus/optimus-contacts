export default class OptimusContactsEmployeeAddModal
{
	constructor(target, params) 
	{
		this.target = target
		this.component = params
	}

	async init()
	{
		await load('/services/optimus-contacts/contacts/employee_add_modal.html', this.target)
		await load('/components/optimus_form.js', modal.querySelector('.employee-block'))
		modal.querySelector('.employee-arrival').value = new Date().toISOString().slice(0, 10)
		setTimeout(() => modal.querySelector('.employee').focus(), 10)

		modal.querySelector('.user-edit.button').onclick = () => 
		{
			if (modal.querySelector('.employee').dataset.id == 'null')
				return false
			modal.hide()
			router('optimus-contacts/contacts/editor?owner=' + this.component.owner + '&id=' + modal.querySelector('.employee').dataset.id + '#general')
		}

		modal.querySelector('.user-create.button').onclick = () =>
			rest('https://' + this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts', 'POST')
				.then(response => 
				{
					if (response.code == 201)
					{
						modal.querySelector('.employee').value = response.data.displayname
						modal.querySelector('.employee').dataset.new_id = response.data.id
						modal.hide()
						router('optimus-contacts/contacts/editor?owner=' + this.component.owner + '&id=' + modal.querySelector('.employee').dataset.new_id + '#general')
					}
				})

		modal.onshow = () => setTimeout(() =>
			rest('https://' + this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts/' + (modal.querySelector('.employee').dataset.new_id || modal.querySelector('.employee').dataset.id))
				.then(response => 
				{
					modal.querySelector('.employee').value = response.data.displayname
					modal.querySelector('.employee').dataset.id = modal.querySelector('.employee').dataset.new_id
					modal.querySelector('.employee').classList.remove('is-danger')
				}), 50)

		modal.querySelector('.add-button').onclick = () =>
		{
			let data =
			{
				contact: this.component.id,
				employee: modal.querySelector('.employee').dataset.id,
				job_title: modal.querySelector('.employee-job-title').value,
				arrival: modal.querySelector('.employee-arrival').value,
				departure: modal.querySelector('.employee-departure').value,
			}
			rest(this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts/' + this.component.id + '/employees', 'POST', data, 'toast')
				.then(response => 
				{
					if (response.code == 201)
					{
						this.component.add_employee(response.data, true)
						modal.close()
					}
				})
		}

		modal.querySelector('.employee-arrival-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.employee-arrival') })
		modal.querySelector('.employee-departure-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.employee-departure') })

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}