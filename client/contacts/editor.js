export default class OptimusContactsTabs
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		loader(this.target)
		let response = await rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id)
		if (response.code == 404)
			return load('/404.html', main) && loader(this.target, false)
		await load('/services/optimus-contacts/contacts/editor.html', main)
		main.querySelector('.title').innerText = response.data.displayname.toUpperCase()

		if (response.data?.restrictions?.includes('delete'))
			this.target.querySelector('.delete-button').classList.add('is-hidden')

		let tabs =
			[
				{
					id: "tab_general",
					text: "Général",
					link: "/services/optimus-contacts/contacts/general.js",
					default: true,
					position: 100
				},
				{
					id: "tab_financier",
					text: "Financier",
					link: "/services/optimus-contacts/contacts/financial.js",
					position: 200
				},
				{
					id: "tab_composition",
					text: "Composition",
					link: "/services/optimus-contacts/contacts/composition.js",
					position: 210,
					class: 'is-hidden'
				},
				{
					id: "tab_relationships",
					text: "Relations",
					link: "/services/optimus-contacts/contacts/relationships.js",
					position: 211,
				},
				{
					id: "tab_notes",
					text: "Notes",
					link: "/services/optimus-contacts/contacts/notes.js",
					position: 300
				},
				{
					id: "tab_bodacc",
					text: "BODACC",
					link: "/services/optimus-contacts/contacts/bodacc.js",
					position: 400,
					class: "is-hidden"
				},
				{
					id: "tab_links",
					text: "Liens utiles",
					link: "/services/optimus-contacts/contacts/links.js",
					position: 500
				},
				{
					id: "tab_plan",
					text: "Plan",
					link: "/services/optimus-contacts/contacts/plan.js",
					position: 600
				},
				{
					id: "tab_authorizations",
					text: "Autorisations",
					link: "/services/optimus-contacts/contacts/authorizations.js",
					position: 700
				}
			]

		let owner_services = (this.owner != store.user.id) ? await rest(store.user.server + '/optimus-base/users/' + this.owner + '/services').then(response => response.data.map(service => service.name)) : store.services.map(service => service.name)
		store.services.map(service => 
		{
			if (owner_services.includes(service.name) && service.optimus_contacts_editor_tabs)
				tabs = tabs.concat(service.optimus_contacts_editor_tabs)
		})

		await load('/components/optimus_tabs.js', this.target,
			{
				router: true,
				tabs_container: document.getElementById('contact_editor_tabs'),
				content_container: document.getElementById('contact_editor_tabs_container'),
				tabs: tabs,
				params: response
			})

		document.querySelector('.delete-button').onclick = () => modal.open('/services/optimus-contacts/contacts/delete_confirmation_modal.js')

		loader(main, false)
	}
}