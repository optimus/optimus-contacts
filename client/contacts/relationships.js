export default class OptimusContactsRelationShipsTab
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		await load('/services/optimus-contacts/contacts/relationships.html', this.target)

		let contact = await rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id).then(response => response.data)

		if (contact.type == 25)
		{
			rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/relatives')
				.then(response => 
				{
					for (let relative of response.data)
						this.add_relative(relative)
					this.target.querySelector('.relatives-block').classList.remove('is-hidden')
					this.target.querySelector('.relative-add-button').onclick = () => modal.open('/services/optimus-contacts/contacts/relative_add_modal.js', false, this)
				})
			rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/employers')
				.then(response => 
				{
					for (let employer of response.data)
						this.add_employer(employer)
					this.target.querySelector('.employers-block').classList.remove('is-hidden')
					this.target.querySelector('.employer-add-button').onclick = () => modal.open('/services/optimus-contacts/contacts/employer_add_modal.js', false, this)
				})
		}
		rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/employees')
			.then(response => 
			{
				for (let employee of response.data)
					this.add_employee(employee)
				this.target.querySelector('.employees-block').classList.remove('is-hidden')
				this.target.querySelector('.employee-add-button').onclick = () => modal.open('/services/optimus-contacts/contacts/employee_add_modal.js', false, this)
			})
	}

	async add_relative(relative, animation)
	{
		const template = document.getElementById('relative').content.firstElementChild.cloneNode(true)

		for (const element of template.querySelectorAll("input,select,textarea"))
			element.setAttribute('data-endpoint', this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/relatives/' + relative.id)
		await populate(template.querySelector('[data-field="type"]'), 'https://' + this.server + '/optimus-contacts/constants/contacts_relatives_types', false, false, relative.type)
		await load('/components/optimus_form.js', template, relative)

		template.querySelector('[data-field="displayname"]').onmouseover = function () { this.classList.add('is-underlined') }
		template.querySelector('[data-field="displayname"]').onmouseleave = function () { this.classList.remove('is-underlined') }
		template.querySelector('[data-field="displayname"]').onclick = () => router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + relative.relative + '#general')

		template.querySelector('.relative-startend-block').classList.toggle('is-invisible', relative.type == 10)
		template.querySelector('.relative-type').onchange = event => template.querySelector('.relative-startend-block').classList.toggle('is-invisible', event.target.value == 10)

		template.querySelector('.relative-delete-button').onmouseover = function () { template.querySelector('.message-header').classList.add('has-background-danger') }
		template.querySelector('.relative-delete-button').onmouseleave = function () { template.querySelector('.message-header').classList.remove('has-background-danger') }

		template.querySelector('.relative-delete-button').onclick = () =>
			rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/relatives/' + relative.id, 'DELETE')
				.then(response => 
				{
					if (response.code == 200)
					{
						template.classList.add('animate__animated', 'animate__zoomOut', 'animate__faster')
						template.addEventListener('animationend', () => template.remove())
					}
				})

		if (animation)
			template.classList.add('animate__animated', 'animate__zoomIn', 'animate__faster')

		main.querySelector('.relatives').appendChild(template)
	}

	async add_employer(employer, animation)
	{
		const template = document.getElementById('employer').content.firstElementChild.cloneNode(true)

		for (const element of template.querySelectorAll("input,select,textarea"))
			element.setAttribute('data-endpoint', this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/employers/' + employer.id)
		await load('/components/optimus_form.js', template, employer)

		template.querySelector('[data-field="displayname"]').onmouseover = function () { this.classList.add('is-underlined') }
		template.querySelector('[data-field="displayname"]').onmouseleave = function () { this.classList.remove('is-underlined') }
		template.querySelector('[data-field="displayname"]').onclick = () => router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + employer.employer + '#general')

		template.querySelector('.employer-delete-button').onmouseover = function () { template.querySelector('.message-header').classList.add('has-background-danger') }
		template.querySelector('.employer-delete-button').onmouseleave = function () { template.querySelector('.message-header').classList.remove('has-background-danger') }

		template.querySelector('.employer-delete-button').onclick = () =>
			rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/employers/' + employer.id, 'DELETE')
				.then(response => 
				{
					if (response.code == 200)
					{
						template.classList.add('animate__animated', 'animate__zoomOut', 'animate__faster')
						template.addEventListener('animationend', () => template.remove())
					}
				})

		if (animation)
			template.classList.add('animate__animated', 'animate__zoomIn', 'animate__faster')

		main.querySelector('.employers').appendChild(template)
	}

	async add_employee(employee, animation)
	{
		const template = document.getElementById('employee').content.firstElementChild.cloneNode(true)

		for (const element of template.querySelectorAll("input,select,textarea"))
			element.setAttribute('data-endpoint', this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/employees/' + employee.id)
		await load('/components/optimus_form.js', template, employee)

		template.querySelector('[data-field="displayname"]').onmouseover = function () { this.classList.add('is-underlined') }
		template.querySelector('[data-field="displayname"]').onmouseleave = function () { this.classList.remove('is-underlined') }
		template.querySelector('[data-field="displayname"]').onclick = () => router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + employee.employee + '#general')

		template.querySelector('.employee-delete-button').onmouseover = function () { template.querySelector('.message-header').classList.add('has-background-danger') }
		template.querySelector('.employee-delete-button').onmouseleave = function () { template.querySelector('.message-header').classList.remove('has-background-danger') }

		template.querySelector('.employee-delete-button').onclick = () =>
			rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id + '/employees/' + employee.id, 'DELETE')
				.then(response => 
				{
					if (response.code == 200)
					{
						template.classList.add('animate__animated', 'animate__zoomOut', 'animate__faster')
						template.addEventListener('animationend', () => template.remove())
					}
				})

		if (animation)
			template.classList.add('animate__animated', 'animate__zoomIn', 'animate__faster')

		main.querySelector('.employees').appendChild(template)
	}

	onUnload()
	{
		delete store.history
		main.querySelector('.undo-button').classList.add('is-hidden')
	}
}