export default class OptimusContactsEmployerAddModal
{
	constructor(target, params) 
	{
		this.target = target
		this.component = params
	}

	async init()
	{
		await load('/services/optimus-contacts/contacts/employer_add_modal.html', this.target)
		await load('/components/optimus_form.js', modal.querySelector('.employer-block'))
		modal.querySelector('.employer-arrival').value = new Date().toISOString().slice(0, 10)
		setTimeout(() => modal.querySelector('.employer').focus(), 10)

		modal.querySelector('.user-edit.button').onclick = () => 
		{
			if (modal.querySelector('.employer').dataset.id == 'null')
				return false
			modal.hide()
			router('optimus-contacts/contacts/editor?owner=' + this.component.owner + '&id=' + modal.querySelector('.employer').dataset.id + '#general')
		}

		modal.querySelector('.user-create.button').onclick = () =>
			rest('https://' + this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts', 'POST')
				.then(response => 
				{
					if (response.code == 201)
					{
						modal.querySelector('.employer').value = response.data.displayname
						modal.querySelector('.employer').dataset.new_id = response.data.id
						modal.hide()
						router('optimus-contacts/contacts/editor?owner=' + this.component.owner + '&id=' + modal.querySelector('.employer').dataset.new_id + '#general')
					}
				})

		modal.onshow = () => setTimeout(() =>
			rest('https://' + this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts/' + (modal.querySelector('.employer').dataset.new_id || modal.querySelector('.employer').dataset.id))
				.then(response => 
				{
					modal.querySelector('.employer').value = response.data.displayname
					modal.querySelector('.employer').dataset.id = modal.querySelector('.employer').dataset.new_id
					modal.querySelector('.employer').classList.remove('is-danger')
				}), 50)

		modal.querySelector('.add-button').onclick = () =>
		{
			let data =
			{
				employee: this.component.id,
				employer: modal.querySelector('.employer').dataset.id,
				job_title: modal.querySelector('.employer-job-title').value,
				arrival: modal.querySelector('.employer-arrival').value,
				departure: modal.querySelector('.employer-departure').value,
			}
			rest(this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts/' + this.component.id + '/employers', 'POST', data, 'toast')
				.then(response => 
				{
					if (response.code == 201)
					{
						this.component.add_employer(response.data, true)
						modal.close()
					}
				})
		}

		modal.querySelector('.employer-arrival-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.employer-arrival') })
		modal.querySelector('.employer-departure-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.employer-departure') })

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}