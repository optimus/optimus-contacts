export default class contactDeleteConfirm
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/services/optimus-contacts/contacts/delete_confirmation_modal.html', this.target)

		modal.querySelector('.delete-button').onclick = () =>
		{
			rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id, 'DELETE', null, 'toast')
				.then(response => 
				{
					if (response.code == 200)
					{
						router('optimus-contacts/contacts')
						modal.close()
					}
				})
		}

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}