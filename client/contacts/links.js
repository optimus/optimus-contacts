export default class contactUsefulLinks
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/services/optimus-contacts/contacts/links.html', this.target, null, true)
		this.target.querySelectorAll('.box').forEach(object => loader(object, true, 'list'))
		rest(store.user.server + '/optimus-contacts/' + store.queryParams.owner + '/contacts/' + store.queryParams.id, 'GET', {})
			.then((data) =>
			{
				const infos = data.data

				this.target.querySelectorAll('.box').forEach(object => loader(object, false))

				document.getElementById('web-button').onclick = () => window.open('https://duckduckgo.com/?q=' + (infos.firstname ? infos.firstname : '') + ' ' + (infos.lastname ? infos.lastname : '') + ' ' + (infos.company_name ? infos.company_name : '') + ' ' + (infos.city_name ? infos.city_name : ''))

				if (infos.siren)
				{
					document.getElementById('siren-warning').style.display = 'none'
					document.getElementById('greffe-button').onclick = () => window.open('https://www.infogreffe.fr/entreprise/' + infos.siren)
					document.getElementById('rcs-button').onclick = () => window.open('https://www.societe.ninja/data.php?siren=' + infos.siren)
				}
				else
				{
					const list = (document.querySelectorAll('.need-siren'))
					for (let el of list)
						el.setAttribute('disabled', '')
				}
				document.getElementById('pj-button').onclick = function ()
				{
					if (infos.type == 30)
						window.open('https://www.pagesjaunes.fr/recherche/' + infos.city_name + "-" + infos.zipcode.substring(0, 2) + "/" + infos.company_name.toLowerCase().replace(' ', '-'))
					else
						window.open('https://www.pagesjaunes.fr/pagesblanches/recherche?quoiqui=' + infos.firstname + "%20" + infos.lastname + "&ou=" + infos.city_name + '&proximite=0')
				}
			})
	}
}