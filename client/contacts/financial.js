export default class contactFinancialInfo
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/services/optimus-contacts/contacts/financial.html', this.target, null, true)
		this.target.querySelectorAll('.box').forEach(object => loader(object, true, 'list'))

		const inputCollection = document.querySelectorAll("input,select")
		for (const el of inputCollection)
		{
			el.setAttribute('data-endpoint', '{server}/optimus-contacts/{owner}/contacts/{id}')
			el.setAttribute('data-field', el.id)
		}

		let response = await rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id)

		await load('/components/optimus_form.js', main, response.data)

		this.target.querySelectorAll('.box').forEach(object => loader(object, false))
	}

	onUnload()
	{
		delete store.history
		main.querySelector('.undo-button').classList.add('is-hidden')
	}
}
