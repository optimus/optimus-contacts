export default class OptimusContacts
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/components/optimus_table/index.js', this.target,
			{
				id: 'contacts',
				version: 2,
				title: 'Contacts',
				resource: 'contacts',
				addIcon: 'fa-user-plus',
				url: 'https://' + store.user.server + '/optimus-contacts/owner/contacts',
				striped: true,
				bordered: true,
				columnSeparator: true,
				fullscreen: true,
				tabulator:
				{
					progressiveLoad: 'auto',
					columns:
						[
							{ field: 'id', title: 'ID', sorter: 'number', sorterParams: { type: 'integer' } },
							{ field: 'displayname', title: 'Nom / Dénomination', width: 350 },
							{ field: 'categorie_description', title: 'Catégorie' },
							{ field: 'type_description', title: 'Type' },
							{ field: 'birth_date', title: 'Naissance', sorter: 'date', sorterParams: { type: 'date' }, formatter: cell => cell.getValue() ? new Date(cell.getValue()).toLocaleDateString('fr') : null, hozAlign: 'center', visible: false },
							{ field: 'company_type_description', title: 'Forme', visible: false },
							{ field: 'siren', title: 'SIREN', hozAlign: 'center' },
							{ field: 'address', title: 'Adresse' },
							{ field: 'zipcode', title: 'CP', sorter: 'number' },
							{ field: 'city_name', title: 'Ville' },
							{ field: 'pays_description', title: 'Pays', width: 150 },
							{ field: 'phone', title: 'Téléphone' },
							{ field: 'fax', title: 'Fax', visible: false },
							{ field: 'mobile', title: 'Portable' },
							{ field: 'email', title: 'Email', width: 150 },
							{ field: 'website', title: 'Site', visible: false },
							{ field: 'pro_phone', title: 'Tél Pro', visible: false },
							{ field: 'pro_fax', title: 'Fax Pro', visible: false },
							{ field: 'pro_mobile', title: 'Portable pro', visible: false },
							{ field: 'pro_email', title: 'Email pro', visible: false },
							{ field: 'pro_website', title: 'Site pro', visible: false }
						],
				},
				rowClick: (event, row) => router('optimus-contacts/contacts/editor?owner=' + (store.queryParams.owner || store.user.id) + '&id=' + row.getData().id + '#general'),
				addClick: () => rest(store.user.server + '/optimus-contacts/' + store.user.id + '/contacts', 'POST', {}, 'toast').then(data => router('optimus-contacts/contacts/editor?owner=' + store.user.id + '&id=' + data.data.id + '#general')),
			})
	}
}