export default class OptimusContactsRelativeAddModal
{
	constructor(target, params) 
	{
		this.target = target
		this.component = params
	}

	async init()
	{
		await load('/services/optimus-contacts/contacts/relative_add_modal.html', this.target)
		await load('/components/optimus_form.js', modal.querySelector('.relative-block'))
		await populate(modal.querySelector('.relative-type'), 'https://' + this.component.server + '/optimus-contacts/constants/contacts_relatives_types')
		setTimeout(() => modal.querySelector('.relative').focus(), 10)

		modal.querySelector('.user-edit.button').onclick = () => 
		{
			if (!modal.querySelector('.relative').dataset.id)
				return false
			modal.hide()
			router('optimus-contacts/contacts/editor?owner=' + this.component.owner + '&id=' + modal.querySelector('.relative').dataset.id + '#general')
		}

		modal.querySelector('.user-create.button').onclick = () =>
			rest('https://' + this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts', 'POST')
				.then(response => 
				{
					if (response.code == 201)
					{
						modal.querySelector('.relative').value = response.data.displayname
						modal.querySelector('.relative').dataset.new_id = response.data.id
						modal.hide()
						router('optimus-contacts/contacts/editor?owner=' + this.component.owner + '&id=' + modal.querySelector('.relative').dataset.new_id + '#general')
					}
				})

		modal.querySelector('.relative-type').onchange = event => modal.querySelector('.relative-startend-block').classList.toggle('is-invisible', event.target.value == 10)

		modal.onshow = () => setTimeout(() =>
			rest('https://' + this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts/' + (modal.querySelector('.relative').dataset.new_id || modal.querySelector('.relative').dataset.id))
				.then(response => 
				{
					modal.querySelector('.relative').value = response.data.displayname
					modal.querySelector('.relative').dataset.id = modal.querySelector('.relative').dataset.new_id
					modal.querySelector('.relative').classList.remove('is-danger')
				}), 50)

		modal.querySelector('.add-button').onclick = () =>
		{
			let data =
			{
				contact: this.component.id,
				relative: modal.querySelector('.relative').dataset.id,
				type: modal.querySelector('.relative-type').value,
			}
			rest(this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts/' + this.component.id + '/relatives', 'POST', data, 'toast')
				.then(response => 
				{
					if (response.code == 201)
					{
						this.component.add_relative(response.data, true)
						modal.close()
					}
				})
		}

		modal.querySelector('.relative-start-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.relative-start') })
		modal.querySelector('.relative-end-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.relative-end') })


		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}