export default class contactGeneral
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
		return this
	}

	async init()
	{
		await load('/services/optimus-contacts/contacts/general.html', this.target, null, true)
		loader(this.target, true, 'list')
		const inputCollection = this.target.querySelectorAll("input,select,textarea")
		for (const el of inputCollection)
		{
			el.setAttribute('data-endpoint', '{server}/optimus-contacts/{owner}/contacts/{id}')
			el.setAttribute('data-field', el.id)
		}
		this.response = await rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id)

		await populate(this.target.querySelector('#categorie'), store.user.server + '/optimus-contacts/constants/contacts_categories', true, true)
		await populate(this.target.querySelector('#type'), store.user.server + '/optimus-contacts/constants/contacts_types', true, false)
		await populate(this.target.querySelector('#country'), store.user.server + '/optimus-contacts/constants/pays', true, false)
		await populate(this.target.querySelector('#birth_country'), store.user.server + '/optimus-contacts/constants/pays', true, false)
		await populate(this.target.querySelector('#marital_status'), store.user.server + '/optimus-contacts/constants/contacts_marital_statuses', true, true)
		await populate(this.target.querySelector('#company_status'), store.user.server + '/optimus-contacts/constants/contacts_company_statuses', true, true)
		await populate(this.target.querySelector('#company_type'), store.user.server + '/optimus-contacts/constants/company_types_lvl3', true, true)
		await populate(this.target.querySelector('#company_greffe'), store.user.server + '/optimus-contacts/constants/greffes_rcs', true, true)

		let optimusForm = await load('/components/optimus_form.js', this.target, this.response.data)

		Promise.all
			(
				[
					this.change_type(this.response.data.type, false),
					this.change_country(false),
					this.change_birth_country(false),
					this.get_cities(this.target.querySelector('#city'), this.response.data.zipcode, this.response.data.city, false),
					this.get_cities(this.target.querySelector('#birth_city'), this.response.data.birth_zipcode, this.response.data.birth_city, false)
				]
			)
			.then(() => loader(this.target, false, 'list'))

		this.target.querySelector('#firstname').addEventListener("keyup", () => this.update_title())
		this.target.querySelector('#lastname').addEventListener("keyup", () => this.update_title())
		this.target.querySelector('#company_name').addEventListener("keyup", () => this.update_title())

		this.target.querySelector('.phone-icon').addEventListener("click", () => this.make_call(document.getElementById('phone')?.value))
		this.target.querySelector('.pro-phone-icon').addEventListener("click", () => this.make_call(document.getElementById('pro_phone')?.value))

		this.target.querySelector('.mobile-icon').addEventListener("click", () => this.make_call(document.getElementById('mobile')?.value))
		this.target.querySelector('.pro-mobile-icon').addEventListener("click", () => this.make_call(document.getElementById('pro_mobile')?.value))

		this.target.querySelector('.email-icon').addEventListener("click", () => this.target.querySelector('#email').value && router('optimus-webmail/webmail/compose', true, main, { to: this.target.querySelector('#email').value }))
		this.target.querySelector('.pro-email-icon').addEventListener("click", () => this.target.querySelector('#pro_email').value && router('optimus-webmail/webmail/compose', true, main, { to: this.target.querySelector('#pro_email').value }))

		this.target.querySelector('#type').onchange = event => this.change_type(event.target.value, true)

		this.target.querySelector('#birth_zipcode').addEventListener("change", event =>
		{
			if (this.target.querySelector('#birth_country').value == '74')
				this.get_cities(this.target.querySelector('#birth_city'), event.target.value, null, true)
		})

		this.target.querySelector('#birth_city').addEventListener("change", event =>
		{
			if (event.target.options.length >= 1)
			{
				this.target.querySelector('#birth_city_name').value = event.target.options[event.target.selectedIndex].text
				rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id, 'PATCH', { birth_city_name: this.target.querySelector('#birth_city_name').value })
				event.target.setCustomValidity('')
			}
		})

		this.target.querySelector('#birth_country').addEventListener("change", () => this.change_birth_country())

		this.target.querySelector('#zipcode').addEventListener("change", event => this.get_cities(this.target.querySelector('#city'), event.target.value, null, true))

		this.target.querySelector('#city').addEventListener("change", event =>
		{
			if (event.target.options.length >= 1)
			{
				this.target.querySelector('#city_name').value = event.target.options[event.target.selectedIndex].text
				rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id, 'PATCH', { city_name: this.target.querySelector('#city_name').value })
			}
			event.target.setCustomValidity('')
		})

		this.target.querySelector('#country').addEventListener("change", () => this.change_country())

		this.target.querySelector('.rne-import-button').addEventListener("click", () => this.get_rne_infos(this.target.querySelectorAll('.siren')[0].value))

		main.querySelector('.undo-button').onclick = () =>
		{
			optimusForm.undo()
			setTimeout(() =>
			{
				this.change_type(this.target.querySelector('#type').value, false)
				this.change_country(this.target.querySelector('#country').value)
				this.change_birth_country(this.target.querySelector('#birth_country').value)

				this.get_cities(this.target.querySelector('#city'), this.target.querySelector('#zipcode').value, this.target.querySelector('#city').value, false)
				this.get_cities(this.target.querySelector('#birth_city'), this.target.querySelector('#birth_zipcode').value, this.target.querySelector('#birth_city').value, false)
			}, 150)
		}
	}

	async get_cities(obj, zipcode, existingCity, save)
	{
		let prefix = obj.id.includes('birth_') ? 'birth_' : ''

		if (document.getElementById(prefix + 'country').value != 74)
			return false

		obj.options.length = 0

		if (!zipcode)
			return false

		if (zipcode == '')
		{
			document.getElementById(obj.id + '_name').value = null
			rest(store.user.server + '/optimus-contacts/' + store.queryParams.owner + '/contacts/' + store.queryParams.id, 'PATCH', { [obj.id]: null, [obj.id + '_name']: null })
			return false
		}

		return rest(store.user.server + '/optimus-contacts/constants/communes', 'GET', { filter: [{ field: "code_postal", type: "=", value: zipcode }, { field: "ancien_nom", type: "=", value: " " }], columns: ["nom", "commune_insee"] })
			.then((results) =>
			{
				const cities = results.data

				if (cities?.length == 0)
				{
					document.getElementById(prefix + 'zipcode').setCustomValidity('Ce code postal est inconnu en France. Veuillez choisir un autre pays ou modifier votre saisie')
					document.getElementById(prefix + 'zipcode').reportValidity()
					return false
				}

				if (cities?.length == 1)
				{
					let opt = document.createElement("option")
					opt.value = cities[0].commune_insee
					opt.text = cities[0].nom
					obj.add(opt)

					if (save === true)
						obj.dispatchEvent(new Event('change'))
				}
				else if (cities.length > 1)
				{
					obj.add(document.createElement("option"))
					for (let city of cities)
					{
						let opt = document.createElement("option")
						opt.value = city.commune_insee
						opt.text = city.nom
						obj.add(opt)
					}
					if (existingCity)
					{
						obj.value = existingCity
						if (save === true)
							obj.dispatchEvent(new Event('change'))
					}
					else if (save === true)
					{
						setTimeout(() => 
						{
							obj.setCustomValidity('Veuillez selectionner la ville dans le menu déroulant')
							obj.reportValidity()
						}, 150)
						obj.dispatchEvent(new Event('change'))
					}
				}
				return true
			})
	}

	async get_rne_infos(siren)
	{
		if (!siren)
			return this.target.querySelectorAll('.siren').forEach(input => 
			{
				input.setCustomValidity('Le numéro SIREN doit être renseigné')
				input.reportValidity()
			})

		if (siren.length != 9)
			return this.target.querySelectorAll('.siren').forEach(input => 
			{
				input.setCustomValidity('Le numéro doit contenir 9 chiffres sans espace')
				input.reportValidity()
			})

		const sireneCall = await fetch('https://recherche-entreprises.api.gouv.fr/search?q=' + siren)

		if (!sireneCall.ok)
			return optimusToast('Le Répertoire National des Entreprises ne répond pas. Veuillez réessayer ultérieurement', 'is-warning')

		const sirene = await sireneCall.json()

		if (sirene.results.length == 0)
			return optimusToast('Aucun résultat pour ce numéro SIREN', 'is-warning')

		const rne_infos = sirene.results[0]

		if (rne_infos.complements.est_entrepreneur_individuel)
		{
			const indiv = rne_infos.dirigeants[0]

			main.querySelector('.title').innerText = indiv.prenoms.toUpperCase() + ' ' + indiv.nom.toUpperCase()
			this.target.querySelector('#type').value = 40
			this.target.querySelector('#firstname').value = indiv.prenoms
			this.target.querySelector('#lastname').value = indiv.nom
			this.target.querySelector('#country').value = 74
			this.change_country()
			this.target.querySelector('#address').value = (rne_infos.siege.numero_voie ? rne_infos.siege.numero_voie : '') + (rne_infos.siege.indice_repetition ? rne_infos.siege.indice_repetition : '') + (rne_infos.siege.type_voie ? ' ' + rne_infos.siege.type_voie : '') + (rne_infos.siege.libelle_voie ? ' ' + rne_infos.siege.libelle_voie : '') + (rne_infos.siege.complementLocalisation ? ' ' + rne_infos.siege.complemen_adresse : '')
			this.target.querySelector('#zipcode').value = rne_infos.siege.code_postal

			await this.get_cities(this.target.querySelector('#city'), rne_infos.siege.code_postal, rne_infos.siege.commune, false)

			rest(store.user.server + '/optimus-contacts/' + store.queryParams.owner + '/contacts/' + store.queryParams.id, 'PATCH',
				{
					type: 40,
					firstname: indiv.prenoms,
					lastname: indiv.nom,
					country: 74,
					address: (rne_infos.siege.numero_voie ? rne_infos.siege.numero_voie : '') + (rne_infos.siege.indice_repetition ? rne_infos.siege.indice_repetition : '') + (rne_infos.siege.type_voie ? ' ' + rne_infos.siege.type_voie : '') + (rne_infos.siege.libelle_voie ? ' ' + rne_infos.siege.libelle_voie : '') + (rne_infos.siege.complementLocalisation ? ' ' + rne_infos.siege.complemen_adresse : ''),
					zipcode: rne_infos.siege.code_postal,
					city: rne_infos.siege.commune,
					city_name: rne_infos.siege.libelle_commune.toUpperCase()
				})
		}
		else
		{
			main.querySelector('.title').innerText = rne_infos.nom_complet.toUpperCase()
			this.target.querySelector('#type').value = 30
			this.target.querySelector('#firstname').value = ''
			this.target.querySelector('#lastname').value = ''
			this.target.querySelector('#birth_zipcode').value = ''
			this.target.querySelector('#birth_city').value = ''
			this.target.querySelector('#birth_city_name').value = ''
			this.target.querySelector('#marital_status').value = ''
			this.target.querySelector('#insee').value = ''
			this.target.querySelector('#company_name').value = rne_infos.nom_complet
			this.target.querySelector('#company_type').value = rne_infos.nature_juridique
			this.target.querySelector('#country').value = 74
			this.change_country()
			this.target.querySelector('#address').value = (rne_infos.siege.numero_voie ? rne_infos.siege.numero_voie : '') + (rne_infos.siege.indice_repetition ? rne_infos.siege.indice_repetition : '') + (rne_infos.siege.type_voie ? ' ' + rne_infos.siege.type_voie : '') + (rne_infos.siege.libelle_voie ? ' ' + rne_infos.siege.libelle_voie : '') + (rne_infos.siege.complementLocalisation ? ' ' + rne_infos.siege.complemen_adresse : '')
			this.target.querySelector('#zipcode').value = rne_infos.siege.code_postal

			await this.get_cities(this.target.querySelector('#city'), rne_infos.siege.code_postal, rne_infos.siege.commune, false)

			rest(store.user.server + '/optimus-contacts/' + store.queryParams.owner + '/contacts/' + store.queryParams.id, 'PATCH',
				{
					type: 30,
					firstname: '',
					lastname: '',
					birth_zipcode: '',
					birth_city: '',
					birth_city_name: '',
					marital_status: '',
					insee: '',
					company_name: rne_infos.nom_complet,
					company_type: rne_infos.nature_juridique,
					country: 74,
					address: (rne_infos.siege.numero_voie ? rne_infos.siege.numero_voie : '') + (rne_infos.siege.indice_repetition ? rne_infos.siege.indice_repetition : '') + (rne_infos.siege.type_voie ? ' ' + rne_infos.siege.type_voie : '') + (rne_infos.siege.libelle_voie ? ' ' + rne_infos.siege.libelle_voie : '') + (rne_infos.siege.complementLocalisation ? ' ' + rne_infos.siege.complemen_adresse : ''),
					zipcode: rne_infos.siege.code_postal,
					city: rne_infos.siege.commune,
					city_name: rne_infos.siege.libelle_commune.toUpperCase()
				})
		}
		this.change_type(this.target.querySelector('#type').value, false)
	}

	change_country(save = true)
	{
		if (this.target.querySelector('#country').value == 74)
		{
			this.target.querySelector('#city_name_container').style.display = 'none'
			this.target.querySelector('#city_container').style.display = ''
			if (save)
				this.get_cities(this.target.querySelector('#city'), this.target.querySelector('#zipcode').value, this.target.querySelector('#city').value, save)
		}
		else
		{
			this.target.querySelector('#city_name_container').style.display = ''
			this.target.querySelector('#city_container').style.display = 'none'
		}
		return true
	}

	change_birth_country(save = true)
	{
		if (this.target.querySelector('#birth_country').value == 74)
		{
			this.target.querySelector('#birth_city_name_container').style.display = 'none'
			this.target.querySelector('#birth_city_container').style.display = ''
			if (save)
				this.get_cities(this.target.querySelector('#birth_city'), this.target.querySelector('#birth_zipcode').value, this.target.querySelector('#birth_city').value, save)
		}
		else
		{
			this.target.querySelector('#birth_city_name_container').style.display = ''
			this.target.querySelector('#birth_city_container').style.display = 'none'
		}
		return true
	}

	update_title()
	{
		main.querySelector('.title').innerText = ''
		if (this.target.querySelector('#type').value == 25)
			main.querySelector('.title').innerText = [this.target.querySelector('#firstname').value.toUpperCase(), this.target.querySelector('#lastname').value.toUpperCase()].filter(Boolean).join(' ')
		else if (this.target.querySelector('#type').value == 30)
			main.querySelector('.title').innerText = this.target.querySelector('#company_name').value.toUpperCase()
		else if (this.target.querySelector('#type').value == 40)
			main.querySelector('.title').innerText = [this.target.querySelector('#company_name').value.toUpperCase(), this.target.querySelector('#firstname').value.toUpperCase(), this.target.querySelector('#lastname').value.toUpperCase()].filter(Boolean).join(' ')
		if (main.querySelector('.title').innerText == '')
			main.querySelector('.title').innerText = 'CONTACT N°' + this.id
	}

	change_type(value, save)
	{
		main.querySelector('#tab_bodacc').classList.toggle('is-hidden', value == 25)
		main.querySelector('#tab_composition').classList.toggle('is-hidden', value != 30)
		this.target.querySelector('#etatcivil').classList.toggle('is-hidden', value == 30)
		this.target.querySelector('#rcs-pm').classList.toggle('is-hidden', value == 25)
		this.target.querySelector('.company_capital').classList.toggle('is-hidden', value != 30)
		this.target.querySelector('.company_type').classList.toggle('is-hidden', value != 30)

		if (value == 40) //ENTREPRISE INDIVIDUELLE
		{
			this.target.querySelector('#company_type').value = 1000

			if (save)
				rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id, 'PATCH',
					{
						company_capital: '',
						company_type: 1000
					})
					.then(response => main.querySelector('.title').innerText = response.data.displayname.toUpperCase())
			else
				this.update_title()
		}
		else if (value == 30) //PERSONNE MORALE
		{
			this.target.querySelector('#firstname').value = ''
			this.target.querySelector('#lastname').value = ''
			this.target.querySelector('#birth_zipcode').value = ''
			this.target.querySelector('#birth_city').value = ''
			this.target.querySelector('#birth_city_name').value = ''
			this.target.querySelector('#marital_status').value = ''
			this.target.querySelector('#insee').value = ''

			if (save)
				rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id, 'PATCH',
					{
						firstname: '',
						lastname: '',
						birth_zipcode: '',
						birth_city: '',
						birth_city_name: '',
						marital_status: '',
						insee: ''
					})
					.then(response => main.querySelector('.title').innerText = response.data.displayname.toUpperCase())
			else
				this.update_title()
		}
		else if (value == 25) //PERSONNE PHYSIQUE
		{
			this.target.querySelector('#company_capital').value = ''
			this.target.querySelector('#company_name').value = ''
			this.target.querySelector('#company_type').value = ''

			if (save)
				rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id, 'PATCH',
					{
						company_capital: '',
						company_name: '',
						company_type: '',
					})
					.then(response => main.querySelector('.title').innerText = response.data.displayname.toUpperCase())
			else
				this.update_title()
		}
		return true
	}

	make_call(number)
	{
		if (!number)
			return false
		let a = document.createElement('a')
		a.href = 'tel:' + number
		a.click()
		return true
	}

	onUnload()
	{
		delete store.history
		main.querySelector('.undo-button').classList.add('is-hidden')
	}
}