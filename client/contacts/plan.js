export default class contactMap
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init() 
	{
		let user = await rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id, 'GET', {})


		if (user.data.address != null)
		{
			document.getElementById('contact_editor_tabs_container').style.height = document.body.offsetHeight - document.getElementById('contact_editor_tabs_container').offsetTop - parseInt(window.getComputedStyle(main).paddingTop) + 'px'
			user.data.geoaddress = user.data.address.split("\n").filter(line => !line.toUpperCase().includes('CEDEX')).filter(line => !line.toUpperCase().startsWith('CS')).filter(line => !line.toUpperCase().startsWith('BP')) + ',' + user.data.zipcode + ',' + user.data.city_name
			const mymap = await load('/components/optimus_map.js', document.getElementById('contact_editor_tabs_container'),
				{
					GoogleMaps: true,
					GoogleStreetView: true,
					StartPoint: user.data.geoaddress
				})
		}
		else
			await load('/services/optimus-contacts/contacts/plan.html', this.target)
	}
}