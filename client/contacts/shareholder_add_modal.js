export default class OptimusContactsShareholderAddModal
{
	constructor(target, params) 
	{
		this.target = target
		this.component = params
	}

	async init()
	{
		await load('/services/optimus-contacts/contacts/shareholder_add_modal.html', this.target)
		await load('/components/optimus_form.js', modal.querySelector('.shareholder-block'))
		modal.querySelector('.shareholder-arrival').value = new Date().toISOString().slice(0, 10)
		setTimeout(() => modal.querySelector('.shareholder').focus(), 10)

		modal.querySelector('.user-edit.button').onclick = () => 
		{
			if (modal.querySelector('.shareholder').dataset.id == 'null')
				return false
			modal.hide()
			router('optimus-contacts/contacts/editor?owner=' + this.component.owner + '&id=' + modal.querySelector('.shareholder').dataset.id + '#general')
		}

		modal.querySelector('.user-create.button').onclick = () =>
			rest('https://' + this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts', 'POST')
				.then(response => 
				{
					if (response.code == 201)
					{
						modal.querySelector('.shareholder').value = response.data.displayname
						modal.querySelector('.shareholder').dataset.new_id = response.data.id
						modal.hide()
						router('optimus-contacts/contacts/editor?owner=' + this.component.owner + '&id=' + modal.querySelector('.shareholder').dataset.new_id + '#general')
					}
				})

		modal.onshow = () => setTimeout(() =>
			rest('https://' + this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts/' + (modal.querySelector('.shareholder').dataset.new_id || modal.querySelector('.shareholder').dataset.id))
				.then(response => 
				{
					modal.querySelector('.shareholder').value = response.data.displayname
					modal.querySelector('.shareholder').dataset.id = modal.querySelector('.shareholder').dataset.new_id
					modal.querySelector('.shareholder').classList.remove('is-danger')
				}), 50)

		modal.querySelector('.add-button').onclick = () =>
		{
			let data =
			{
				contact: this.component.id,
				shareholder: modal.querySelector('.shareholder').dataset.id,
				arrival: modal.querySelector('.shareholder-arrival').value,
				departure: modal.querySelector('.shareholder-departure').value,
				bare: modal.querySelector('.shareholder-bare').value,
				usufruct: modal.querySelector('.ownership-division').checked ? modal.querySelector('.shareholder-usufruct').value : modal.querySelector('.shareholder-bare').value,
			}
			rest(this.component.server + '/optimus-contacts/' + this.component.owner + '/contacts/' + this.component.id + '/shareholders', 'POST', data, 'toast')
				.then(response => 
				{
					if (response.code == 201)
					{
						this.component.add_shareholder(response.data, true)
						modal.close()
					}
				})
		}

		modal.querySelector('.ownership-division').onchange = event =>
		{
			modal.querySelector('.shares.label').classList.toggle('is-hidden', event.target.checked)
			modal.querySelector('.bare.label').classList.toggle('is-hidden', !event.target.checked)
			modal.querySelector('.usufruct-block').classList.toggle('is-hidden', !event.target.checked)
		}

		modal.querySelector('.shareholder-arrival-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.shareholder-arrival') })
		modal.querySelector('.shareholder-departure-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.shareholder-departure') })

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}