export default class contactShare
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/components/optimus_authorizations/index.js', this.target,
			{
				server: store.user.server,
				url: 'https://{server}/optimus-base/authorizations',

				showTitle: false,
				title: false,
				showSubtitle: false,
				subtitle: null,
				showAddButton: (store.user.admin == false && store.user.id != this.owner) ? false : 'bottom',
				pagination: false,

				striped: true,
				bordered: true,
				columnSeparator: true,

				columns: ['user', 'read', 'write', 'create', 'delete'],
				headerFilterInputs: null,

				filters:
				{
					owner: this.owner,
					user: this.user,
					resource: 'contacts/' + this.id
				},

				editor:
				{
					blocks: ['user', 'rights'],
					read: true,
					write: false,
					delete: false
				}
			})
	}
}